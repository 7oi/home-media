# Home media setup
This is just a simple way to set up a complete home media setup using `docker-compose`.

It includes:
- `radarr` - a movie collection manager
- `sonarr` - a tv show collection manager
- `plex`   - a media server
- `transmission` - a torrent client

## Prerequisites
You must have a machine with `docker` and `docker-compose` installed. That's about it. It will run on just about anything that meets those requirements, as the containers include arm versions (so yes, perfect for a raspberry pi, which was my intended use case).

## Setup
1. Clone this repo
2. Configure env files under `./envs`
   - `common.env`: add user id and such (see comments in env file)
   - `transmission.env`: add desired log in credentials (see comments in env file)
   - `plex.env`: add Plex claim token (see comments in env file)
3. Configure mounted volumes in `docker-compose.yml`. The current setup applies to my own setup, so yours will definitely differ.
4. Run the thing. `docker-compose up -d`
5. Visit the web UI's for all the things and do the traditional setup steps.
   - note the hostnames for the containers. F.ex. when pointing at transmission in sonarr/radarr the host will be `transmission`, as that's it's hostname in relation to the other containers.
